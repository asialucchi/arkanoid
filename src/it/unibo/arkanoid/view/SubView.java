package it.unibo.arkanoid.view;

/**
 * 
 * This enumeration defines a list of possible Scene in the Game.
 *
 */
public enum SubView {

    /**
     * Home scene.
     */
    HOME,

    /**
     * Scores scene.
     */
    SCORES,

    /**
     * Game screen scene.
     */
    IN_GAME,

    /**
     * End of level scene.
     */
    LEVEL_END,

    /**
     * End game scene.
     */
    GAME_FINISHED,

    /**
     * Lose scene.
     */
    GAME_OVER;

}
